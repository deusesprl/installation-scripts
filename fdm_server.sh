#!/bin/bash

# Check if mono-complete package is installed
if ! [ -x "$(command -v mono)" ]; then
  sudo apt-get install -y mono-complete
  sudo apt-get update
fi

sudo ./FDM.Server.GNU-Linux_3_5_14.run
# TODO: Automatically put Y for the 2 inputs asked would be nice
# (but all conventional ways like printf 'y\ny\n' | sudo ./FDM.Server... don't work)