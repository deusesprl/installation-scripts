1. Lancer les conteneurs docker:
    `docker-compose up -d`

2. Vérifier que les dockers sont bien lancés:
    `docker ps`

3. Mettre le conteneur en mode debug:
    `docker-compose stop service && docker-compose run --service-ports --rm service`

(
    build les conteneurs:
        `docker-compose -f prod.yml build`
)



**!! Toute commande exécutant docker-compose doit être faite dans le dossier du projet !!**

