# Scripts d'installation Hiopos Box

Ce répertoire contient tous les scripts bash nécessaires à l'installation et au bon fonctionnement d'une Hiopos Box:



- docker_install.sh : ce script installe toutes les dépendances liées à docker/docker-compose. On utilise pour cela le script officiel de Docker afin de permettre une installation générique sur tout type d'architecture (une raspberry n'étant pas de type x86_64). A la fin du script, on reboote le système pour lui permettre de finaliser la configuration docker (user etc...).



- fdm_server.sh : ce script permet de mettre en place le serveur FDM, qui fera le lien entre l'Hiopos Box et la Boîte noire. Il télécharge un package important puis exécute le script FDM.Server.



- FDM.Server.GNU-Linux_3_5_14.run : Ce script lance la fenêtre de mise en place des serveurs FDM. Après avoir configuré manuellement le serveur, la communication est établie et le process doit fonctionner normalement



- install.sh : ce script clone le projet hiopos_box, configure les paramètres locaux et permet d'installer le serveur flask et de le lancer. Pour s'identifier, l'utilisateur doit saisir un identifiant Hiopos Box, dont l'on va vérifier l'existence dans notre base de données. Si l'on ne trouve rien, un message d'erreur apparaît et on invite la personne à recommencer. Dans le second cas, on va vérifier la présence de paramètres pour l'objet en question. En fonction de l'existence de ces paramètres, on va proposer à l'utilisateur de réaliser une installation soit automatique, soit manuelle.

  Une installation automatique récupère les paramètres de la base de données et instancie directement les paramètres locaux de la Box. Une installation manuelle permet à l'utilisateur de saisir lui-même les paramètres. Après avoir saisi le Pos Serial Number, et sélectionné le service externe, une requête PUT est envoyée au serveur Django pour mettre à jour les paramètres dans la DB.

- docker-commands.md : Ce fichier est une petite documentation très légère recensant les commandes docker importantes à savoir utiliser en cas de difficultés.


Pour installer une Hiopos Box et faire en sorte qu'elle soit complètement fonctionnelle, lancer d'abord le script `docker_install.sh` **à moins que la Hiopos Box n'ait été initialisée à l'aide d'une image système déjà pré-configuré**, puis le script `fdm_server.sh` pour initialiser le serveur FDM communiquant avec la boite noîre. Enfin, exécuter le script `install.sh` pour télécharger le projet, configurer le système et lancer le service.
