#!/bin/bash

set -e

# Docker/Docker-compose installation

# zenity (dialog window tool)
if ! [ -x "$(command -v zenity)" ]; then
  sudo apt-get install -y zenity
  sudo apt-get update
fi

echo -e "\n**** Docker installation: this step can take some time. Please don't interrupt the process. ****\n"
sleep 2
if ! [ -x "$(command -v docker)" ]; then
  # Docker basic packages installation
  sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
  sudo apt-get update

  # Offical docker installation script
  curl -sSL https://get.docker.com -o get-docker.sh
  sudo sh get-docker.sh
  rm get-docker.sh

  # Manage Docker as a non-root user
  echo -e "\n**** Create docker user ****\n"
  sudo groupadd -f docker
  sudo usermod -aG docker "$USER"
  sleep 2

  # Change location of Docker files
  echo -e "\n**** Change location of docker files ****\n"
  sudo su -c "
      docker ps -q | xargs -r docker kill;
      service docker stop;
      umount /var/lib/docker/*;
      mv /var/lib/docker $HOME/docker;
      sed -i '/#DOCKER_OPTS=\"--dns 8.8.8.8 --dns 8.8.4.4\"/a DOCKER_OPTS=\"-g $HOME/docker -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock\"' /etc/default/docker;
      ln -s /home/pi/docker /var/lib/docker;
      service docker start;
  "
  sleep 2

  echo -e "\n**** Finishing docker installation ****\n"
  sleep 2
else
  echo -e "\n**** Docker already installed: pursuing with docker-compose installation... ****\n"
  sleep 2
fi

echo -e "\n**** Docker-compose installation: this step can take some time. Please don't interrupt the process. ****\n"
sleep 2
if ! [ -x "$(command -v docker-compose)" ]; then
  # Docker-compose installation
  sudo apt-get install -y libffi-dev libssl-dev python3 python3-pip
  sudo apt-get remove -y python-configparser
  sudo apt-get update
  sudo pip3 install -v docker-compose

  # Change temporary folder of docker-compose build
  echo -e "\n**** Change temporary folder of docker-compose build files ****\n"
  cd ~ || exit
  echo -e "\n# Docker-compose build temporary files config\nmkdir -p $HOME/tmp\nexport TMPDIR=$HOME/tmp" >> "$HOME/.bashrc"
  source "$HOME"/.bashrc
  sleep 2

  echo -e "\n**** Finishing docker-compose installation ****\n"
  sleep 2
else
  echo -e "\n**** Docker-compose already installed: exiting the program... ****\n"
  sleep 2
  exit 0
fi

zenity --info --width 300 \
--text "Configuration done. The Hiopos Box will now reboot to update its parameters. Click OK to continue." \
--title "Hiopos Box Configuration" 2>/dev/null
reboot
exit 0
