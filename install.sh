#!/bin/bash
set -o pipefail

# Basic packages installation
  # zenity (dialog window tool)
if ! [ -x "$(command -v zenity)" ]; then
  sudo apt-get install -y zenity
fi

# Global variables
# BASE_URL="http://hiopostoblackbox.local:8000/api"
BASE_URL="https://hiopostoblackbox.deuse.live/api"
if [ $# -eq  0 ]
then PROJECT_NAME="hiopos_box"
else PROJECT_NAME=$1
fi
ID_REGEX="^[a-zA-Z0-9]+$"
POS_SERIAL_NUMBER_REGEX="^[A-Z0-9]{14}$"

LANGUAGE=$(zenity --entry --title="Hiopos Box Installation" --text="Select the language you prefer" \
  English Nederlands --entry-text="Français" --width=400 --height=200 2>/dev/null)

if [ $? -eq 1 ]
then
  zenity --error --title "Hiopos Box Installation" --text="Hiopos Box installation canceled." --width=400 --height=200 2>/dev/null
  exit 1

elif [ "$LANGUAGE" = "English" ]
then

  MESSAGES=("Hiopos Box Installation" "You are going to exit the installation process. Are you sure you want to continue ?" "Hiopos Box installation canceled."
  "Installing Docker architecture (if it's the first installation ever on the Hiopos Box, this step can take up to 10 minutes)" "Installation done. Click OK to continue."
  "Docker not installed: stopping the installation process. You must first execute the docker_install.sh file." "# Initializing installation process"
  "# Downloading Hiopos Box project (this step can take some time)" "Hiopos Box: an internal error has been encountered. Please try again later or contact the Hiopos Box administation."
  "# Setting local ID" "# Setting local url of your Hiopos Box" "# Getting eventual information about your Hiopos Box" 
  "Our database already contains configuration information for your Hiopos Box. Do you want to keep these information or reconfigure everything manually ?"
  "# Hiopos Box IP address configuration" "# Finishing Hiopos Box configuration")

  LOCAL_ID_MESSAGES=("Please enter your Hiopos Box ID" "Error with the Hiopos Box ID: the ID can only contains numbers and lowercase/uppercase letters. Please, try again"
  "The Hiopos Box ID doesn't exist in our database: please enter an other ID or contact the administration")

  AUTOMATIC_CONFIG_MESSAGES=("# Setting the Point Of Service serial number" "# Setting external communication channel" "# Configuration of the external service credentials")

  MANUAL_CONFIG_MESSAGES=("# Setting the Point Of Service serial number" "Enter the Point Of Service serial number (14 characters long)" 
  "Error with the Point Of Service serial number: the number can only contain 14 numbers/uppercase letters. Please try again" "# Setting external communication channel" 
  "Select the service you want to communicate with" "# Configuration of the external service credentials" "Please enter your Black Box credentials" 
  "Username" "Password" "Error: the FDM server seems unactive. Please install or start the server first." "Error: your credentials are invalid. Please try again")

elif [ "$LANGUAGE" = "Français" ]
then

  MESSAGES=("Installation de l'Hiopos Box" "Vous allez quitter le processus d'installation. Êtes-vous sûr de vouloir continuer ?" "Installation de l'Hiopos Box annulée."
  "Installation et configuration de l'architecture Docker (si c'est la toute première installation sur l'Hiopos Box, cette étape peut prendre jusqu'à 10 minutes)" "Installation réussie. Cliquez OK pour continuer."
  "Docker non installé: arrêt du processus. Vous devez d'abord exécuter le fichier docker_install.sh." "# Initialisation du processus d'installation"
  "# Téléchargement du projet Hiopos Box (cette étape peut prendre un peu de temps)" "Hiopos Box: une erreur interne a été rencontré. Veuillez recommencer plus tard ou bien contactez l'administration Hiopos Box."
  "# Instantiation de l'ID local" "# Configuration de l'url local dans votre Hiopos Box" "# Récupération d'éventuelles informations à propos de votre Hiopos Box"
  "Notre base de données contient déjà des informations de configuration pour votre Hiopos Box. Voulez-vous garder ces informations ou tout reconfigurer manuellement ?"
  "# Configuration de l'adresse IP Hiopos Box" "# Finition du paramétrage de l'Hiopos Box")

  LOCAL_ID_MESSAGES=("Veuillez saisir votre ID Hiopos Box" "Erreur avec l'ID Hiopos Box: l'ID ne peut contenir que des chiffres et des lettres minuscules/majuscules. Veuillez recommencer"
  "L'ID Hiopos Box saisi n'existe pas dans notre base de données: veuillez saisir un autre ID ou contacter l'administration.")

  AUTOMATIC_CONFIG_MESSAGES=("# Configuration du numéro du Point de Service du système" "# Configuration du canal de communication avec les services externes"
  "# Configuration des identifiants du service externe")

  MANUAL_CONFIG_MESSAGES=("# Configuration du numéro du Point De Service du système" "Veuillez saisir le numéro du Point de Service du système (chaîne de 14 caractères)"
  "Erreur avec le numéro du Point de Service: la chaîne ne peut contenir que 14 chiffres/lettres majuscules. Veuillez recommencer"
  "# Configuration du canal de communication avec les services externes" "Sélectionnez le service avec lequel vous souhaitez communiquer" 
  "# Configuration des identifiants du service externe" "Veuillez saisir vos identifiants de connexion Boîte noire" "Nom d'utilisateur" "Mot de passe"
  "Erreur : le serveur FDM semble inactif. Veuillez installer ou démarrer le serveur en premier." "Erreur: vos identifiants de connexion sont incorrects. Veuillez recommencer")

else

  MESSAGES=("Hiopos Box Installatie" "U verlaat het installatieproces. Weet je zeker dat je wilt doorgaan ?" "Hiopos Box installatie geannuleerd."
  "Docker-architectuur installeren (als dit de allereerste installatie op de Hiopos Box is, kan deze stap tot 10 minuten duren)" "Installatie voltooid. Klik op OK om door te gaan."
  "Docker niet geïnstalleerd: het installatieproces stoppen. U moet eerst het docker_install.sh bestand uitvoeren." "# Installatieproces initialiseren"
  "# Hiopos Box project downloaden (deze stap kan een beetje tijd in beslag nemen)" "Hiopos Box: een interne fout werd aangetroffen. Probeer het later nog eens of neem contact op met de Hiopos Box administratie."
  "# Lokale ID instellen" "# Lokale url van uw Hiopos Box instellen" "# Mogelijke informatie over uw Hiopos Box opvragen"
  "Onze database bevat al configuratie-informatie voor uw Hiopos Box. Wil je die informatie bewaren of alles handmatig herconfigureren ?"
  "# Configuratie van het Hiopos Box IP-adres" "# Afwerking Hiopos Box configuratie")

  LOCAL_ID_MESSAGES=("Voer uw Hiopos Box ID in" "Fout met de Hiopos Box ID: de ID kan alleen cijfers en kleine/grote letters bevatten. Probeer het nog eens"
  "De ingevoerde Hiopos Box ID bestaat niet in onze database: voer een andere ID in of neem contact op met de administratie.")

  AUTOMATIC_CONFIG_MESSAGES=("# Configuratie van het Service Punt nummer van het systeem" "# Configuratie van het communicatiekanaal met externe diensten" 
  "# Configuratie van de externe service identifiers")

  MANUAL_CONFIG_MESSAGES=("# Configuratie van het Service Punt nummer van het systeem" "Voer het Service Punt nummer van het systeem in (14 tekens)."
  "Fout met het Service Punt nummer: de string kan slechts 14 cijfers/hoofdletters bevatten. Probeer het nog eens." "# Configuratie van het communicatiekanaal met externe diensten"
  "Selecteer de afdeling waarmee u wilt communiceren" "# Configuratie van de externe service identifiers" "Voer uw Black Box inloggegevens in" 
  "Gebruikersnaam" "Wachtwoord" "Fout: De FDM-server lijkt inactief. Installeer of start de server eerst." "Fout: uw inloggegevens zijn onjuist. Probeer het nog eens")
fi

TITLE="${MESSAGES[0]}"
CONFIRM_CANCEL_MESSAGE="${MESSAGES[1]}"
CANCEL_MESSAGE="${MESSAGES[2]}"
DOCKER_BUILD_MESSAGE="${MESSAGES[3]}"
SUCCESS_MESSAGE="${MESSAGES[4]}"

MESSAGE_COUNTER=5 # Counter to increment each time a new message is displayed to the user

(
  # Check if docker and docker-compose commands are known
  if ! [ -x "$(command -v docker)" ] || ! [ -x "$(command -v docker-compose)" ] ; then
    zenity --error --title "$TITLE" --text "${MESSAGES[$MESSAGE_COUNTER]}" --width=300 --height=200 2>/dev/null
    exit 1
  fi
  ((MESSAGE_COUNTER++))

  # Initialize installation process
    # Get Project name
  echo "${MESSAGES[$MESSAGE_COUNTER]}"
  ((MESSAGE_COUNTER++))
    # Install jq package (json manager tool)
  if ! [ -x "$(command -v jq)" ]; then
    sudo apt-get install -y jq
    sudo apt-get update
  fi
  sleep 2

  # Get project folder
  echo "${MESSAGES[$MESSAGE_COUNTER]}"
  ((MESSAGE_COUNTER++))
  # Delete the project folder if it already exists
  if  [ -d "$PROJECT_NAME" ]
  then rm -r "$PROJECT_NAME"
  fi

  # Clone the last stable version repository and remove useless folders/files
  git clone --depth 1 -b master 'https://bitbucket.org/deusesprl/hiopos_box.git' "$PROJECT_NAME"

  if [ ! -d "./$PROJECT_NAME" ]
  then
    zenity --error --title "$TITLE" --text "${MESSAGES[$MESSAGE_COUNTER]}" --width=300 --height=200 2>/dev/null
    exit 1
  fi
  ((MESSAGE_COUNTER++))
  cd "$PROJECT_NAME" || exit 1
  rm -rf .git

  # Instantiate local id and change every occurence of LOCAL_ID by the id entered
  echo "${MESSAGES[$MESSAGE_COUNTER]}"
  ((MESSAGE_COUNTER++)) 
  ID_DB_STATUS="404"
    # Restart the process until the LOCAL_ID value respects the regex defined at the beginning of the script
    # and until we check the ID entered is known from the DB
  while ! [ "$ID_DB_STATUS" = "200" ]; do
    # Let the user enters the ID of its hiopos box
    LOCAL_ID="$(zenity --entry --title "$TITLE" --text "${LOCAL_ID_MESSAGES[0]}" --width=300 --height=200 2>/dev/null)"

    if [[ $? -eq 1 ]]; then
      # Cancel button pressed: we ask if the user wants to cancel or if it was an error
      zenity --question --title "$TITLE" --text "$CONFIRM_CANCEL_MESSAGE" --width=300 --height=200 2>/dev/null
      if [ $? = 0 ]; then
        # Yes button: the user confirms it wants to exit. Exit the pipe
        exit 1
      fi
    elif ! [[ "${LOCAL_ID}" =~ ${ID_REGEX} ]]; then
      # Error regex
      zenity --warning --title "$TITLE" --text "${LOCAL_ID_MESSAGES[1]}" --width=300 --height=200 2>/dev/null
    else
      # Check if the ID exists in the DB
      ID_DB_STATUS="$(curl -s -o /dev/null -I -w "%{http_code}" $BASE_URL/hiopos-box/$LOCAL_ID/box/)"
      if ! [ "$ID_DB_STATUS" = "200" ]; then
        # Error DB: Hiopos Box ID unknown
        zenity --warning --title "$TITLE" --text "${LOCAL_ID_MESSAGES[2]}" --width=300 --height=200 2>/dev/null
      fi
    fi
  done

  # Replace the LOCAL_ID variable into settings file
  sed -i "s/LOCAL_ID/\"$LOCAL_ID\"/g" service/settings.py
  sleep 2

  # Add the local url to the hosts of the raspberry pi
  echo "${MESSAGES[$MESSAGE_COUNTER]}"
  ((MESSAGE_COUNTER++))
  if [ "$(grep -c "$PROJECT_NAME.local" /etc/hosts)" -eq 0 ]
  then sudo sed -i "1 i\127.0.0.1\\t$PROJECT_NAME.local" /etc/hosts
  fi
  sleep 1


  # Check if the Django server already contains info about this Hiopos Box (thanks to the ID)
  echo "${MESSAGES[$MESSAGE_COUNTER]}"
  ((MESSAGE_COUNTER++))
  HIOPOS_BOX_INFO="$(curl -s "$BASE_URL/hiopos-box/$LOCAL_ID/box/" | jq '.')"

    # Get the eventual main info (can't be empty if the user already used a box with this ID)
  POS_SERIAL_NUMBER="$(jq '.pos_serial_number' <<< "$HIOPOS_BOX_INFO" | sed -e 's/^\"//' -e 's/\"$//')"
  EXTERNAL_SERVICE_URL="$(jq '.external_service.url' <<< "$HIOPOS_BOX_INFO" | sed -e 's/^\"//' -e 's/\"$//')"
  TRANSACTION_NUMBER="$(jq '.transaction_number' <<< "$HIOPOS_BOX_INFO")"

  # Check if the info exist or are empty
  INSTALLATION_METHOD="MANUAL" # by default
  if [ "$POS_SERIAL_NUMBER" ] && [ "$EXTERNAL_SERVICE_URL" ]
  then
    # Information found: we ask the user if it wants to keep these info or if it wants to reconfigure everything
    zenity --question --title "$TITLE" --text "${MESSAGES[$MESSAGE_COUNTER]}" --width=300 --height=200 2>/dev/null
    if [ $? = 0 ]; then
      # Yes button: the user wants to keep the info and do an automatic reinstallation
      INSTALLATION_METHOD="AUTOMATIC"
    fi
  fi
  ((MESSAGE_COUNTER++))

  if [ "$INSTALLATION_METHOD" = "AUTOMATIC" ]
  then
      # Setup the pos serial number
    echo "${AUTOMATIC_CONFIG_MESSAGES[0]}"
    sed -i "s,LOCAL_POS_SERIAL_NUMBER,\"$POS_SERIAL_NUMBER\",g" service/settings.py
    sleep 2

      # Setup the external service url
    echo "${AUTOMATIC_CONFIG_MESSAGES[1]}"
    sed -i "s,EXTERNAL_URL,\"$EXTERNAL_SERVICE_URL\",g" service/settings.py
    sleep 2

      # Implement the Black Box credentials info if the user chose the Black Box service
    echo "${AUTOMATIC_CONFIG_MESSAGES[2]}"

      # Check if the external service identifier refers to the Black Box
    SELECTED_SERVICE_ID="$(jq '.external_service.identifier' <<< "$HIOPOS_BOX_INFO" | sed -e 's/^\"//' -e 's/\"$//')"
    if [ "$SELECTED_SERVICE_ID" = "BBX" ]
    then
        # Get the credentials
      USERNAME="$(jq '.username' <<< "$HIOPOS_BOX_INFO" | sed -e 's/^\"//' -e 's/\"$//')"
      PASSWORD="$(jq '.password' <<< "$HIOPOS_BOX_INFO" | sed -e 's/^\"//' -e 's/\"$//')"
      CREDENTIALS="$(echo -n  "$USERNAME:$PASSWORD" | base64)"

        # Add the credentials to the settings file
      sed -i "7i\BLACK_BOX_CREDENTIALS = \"$CREDENTIALS\"" service/settings.py
    fi

      # Setup the transaction number
    sed -i "s,transaction_number = 0,transaction_number = $TRANSACTION_NUMBER,g" service/settings.py

    # Manual configuration
  elif [ "$INSTALLATION_METHOD" = "MANUAL" ]
  then
      # Setup the pos serial number
    echo "${MANUAL_CONFIG_MESSAGES[0]}"

    POS_SERIAL_STATUS="404"
    # Restart the process until the POS_SERIAL_NUMBER value respects the regex defined at the beginning of the script
    while [ "$POS_SERIAL_STATUS" != "200" ]; do
      # Let the user enters the POS Serial Number
      POS_SERIAL_NUMBER="$(zenity --entry --title "$TITLE" --text "${MANUAL_CONFIG_MESSAGES[1]}" --width=300 --height=200 2>/dev/null)"

      if [[ $? -eq 1 ]]; then
        # Cancel button pressed: we ask if the user wants to cancel or if it was an error
        zenity --question --title "$TITLE" --text "$CONFIRM_CANCEL_MESSAGE" --width=300 --height=200 2>/dev/null
        if [ $? = 0 ]; then
          # Yes button: the user confirms it wants to exit. Exit the pipe
          exit 1
        fi
      elif ! [[ "${POS_SERIAL_NUMBER}" =~ ${POS_SERIAL_NUMBER_REGEX} ]]; then
        # Error regex
        zenity --warning --title "$TITLE" --text "${MANUAL_CONFIG_MESSAGES[2]}" --width=300 --height=200 2>/dev/null
      else
        # TODO: The ideal would be to to a call to the fdm server with the pos serial number entered to check if it exists
        # in the black box data base
        POS_SERIAL_STATUS="200"
        # Post the credentials to the django server
        curl -s -X PUT -H "Content-Type: application/json" -d '{"pos_serial_number": "'"$POS_SERIAL_NUMBER"'"}' "$BASE_URL/hiopos-box/$LOCAL_ID/update/" | jq '.'
      fi
    done

      # Replace the pos serial number setting variable
    sed -i "s,LOCAL_POS_SERIAL_NUMBER,\"$POS_SERIAL_NUMBER\",g" service/settings.py
    sleep 1

      # Setup the external service url
    echo "${MANUAL_CONFIG_MESSAGES[3]}"

      # Get external services from the DB
    EXTERNAL_SERVICES="$(curl -s $BASE_URL/external-service/ | jq '.')"

      # Get the length of the EXTERNAL_SERVICES object (number of services in the DB)
    LENGTH="$(jq '. | length' <<< "$EXTERNAL_SERVICES")"

      # Dynamically create an array with the name of all services
    ARRAY=()
    for (( i=0; i < LENGTH; ++i )) do
        OBJECT="$(jq --arg INDEX "$i" '.[$INDEX|tonumber]' <<< "$EXTERNAL_SERVICES")"
        NAME="$(jq '.name' <<< "$OBJECT" | sed -e 's/^\"//' -e 's/\"$//')"
        ARRAY+=("$NAME")
    done

    EXTERNAL_SERVICE=""
      # Display a zenity window to let the user select the service it wants to communicate with
    while [ -z "$EXTERNAL_SERVICE" ]; do
      EXTERNAL_SERVICE="$(zenity --list --title="$TITLE" --text="${MANUAL_CONFIG_MESSAGES[4]}" --column="" "${ARRAY[@]}" 2>/dev/null)"

      if [[ $? -eq 1 ]]; then
        # Cancel button pressed: we ask if the user wants to cancel or if it was an error
        zenity --question --title "$TITLE" --text "$CONFIRM_CANCEL_MESSAGE" --width=300 --height=200 2>/dev/null
        if [ $? = 0 ]; then
          # Yes button: the user confirms it wants to exit. Exit the pipe
          exit 1
        fi
      fi
    done

        # Get the ID of the selected external service
    SELECTED_SERVICE_ID="$(jq --arg NAME "$EXTERNAL_SERVICE" '.[] | select(.name==$NAME) | .identifier' <<< "$EXTERNAL_SERVICES" | sed -e 's/^\"//' -e 's/\"$//')"

      # Get the url of the selected external service
    SELECTED_SERVICE_URL="$(jq --arg NAME "$EXTERNAL_SERVICE" '.[] | select(.name==$NAME) | .url' <<< "$EXTERNAL_SERVICES")"

    sleep 1

      # Implement the Black Box credentials info if the user chose the Black Box service
    echo "${MANUAL_CONFIG_MESSAGES[5]}"

    if [ "$SELECTED_SERVICE_ID" = "BBX" ]
    then
      CREDENTIALS_STATUS="401"
      while [ "$CREDENTIALS_STATUS" = "401" ]; do
        # get the credentials from the user
        CREDENTIALS="$(zenity --forms --title "$TITLE" --text "${MANUAL_CONFIG_MESSAGES[6]}" \
        --add-entry "${MANUAL_CONFIG_MESSAGES[7]}" \
        --add-password "${MANUAL_CONFIG_MESSAGES[8]}" \
        --separator "|" \
        2>/dev/null)"

        if [[ $? -eq 1 ]]; then
          # Cancel button pressed: we ask if the user wants to cancel or if it was an error
          zenity --question --title "$TITLE" --text "$CONFIRM_CANCEL_MESSAGE" --width=300 --height=200 2>/dev/null
          if [ $? = 0 ]; then
            # Yes button: the user confirms it wants to exit. Exit the pipe
            exit 1
          fi
        fi

        # Get the data entered and create the base64 string in order to do a request to the Black Box server
        USERNAME="$(echo -n "$CREDENTIALS" | cut -d "|" -f1 )" # username
        PASSWORD="$(echo -n "$CREDENTIALS" | cut -d "|" -f2 )" # password
        CREDENTIALS="$(echo -n  "$USERNAME:$PASSWORD" | base64)"

        # Request to the fdm server: if the return status code is 401, then the credentials are incorrect. Else, we can pass to the next step
        CREDENTIALS_STATUS="$(curl -s -X POST -H "Authorization: Basic $CREDENTIALS" -d "{}" http://localhost:8858/client/HashAndSign/ | jq '.Errors[0].Code')"

        # Error : server fdm down
        if [ -z "$CREDENTIALS_STATUS" ]
        then
          zenity --error --title "$TITLE" --text "${MANUAL_CONFIG_MESSAGES[9]}" --width=300 --height=200 2>/dev/null
          exit 1

        # Error : wrong credentials
        elif [ "$CREDENTIALS_STATUS" = "401" ]
        then
          zenity --warning --title "$TITLE" --text "${MANUAL_CONFIG_MESSAGES[10]}" --width=300 --height=200 2>/dev/null
        else
          # Post the external service and the credentials into the DB
          # (post of the external service done after credentials validation to avoid the case where the user selects Black box
          # service but don't remember its credentials)          
          curl -s -X PUT -H "Content-Type: application/json" -d '{"external_service": "'"$SELECTED_SERVICE_ID"'", "username": "'"$USERNAME"'", "password": "'"$PASSWORD"'"}' "$BASE_URL/hiopos-box/$LOCAL_ID/update/" | jq '.'

          # Add the credentials to the settings file
          sed -i "7i\BLACK_BOX_CREDENTIALS = \"$CREDENTIALS\"" service/settings.py
        fi

      done
      # end of the credentials status while loop
    
    # Other external service case (no credentials so we can directly update info in the DB)
    else
      # Update the external service of the Hiopos Box into the DB
      curl -s -X PUT -H "Content-Type: application/json" -d '{"external_service": "'"$SELECTED_SERVICE_ID"'"}' "$BASE_URL/hiopos-box/$LOCAL_ID/update/" | jq '.'
    fi
    # end of the external service condition

    # Replace the SERVICE_URL variable into settings file
    sed -i "s,EXTERNAL_URL,$SELECTED_SERVICE_URL,g" service/settings.py

    # Setup the transaction number
    sed -i "s,transaction_number = 0,transaction_number = $TRANSACTION_NUMBER,g" service/settings.py
    sleep 1
  fi
  # end of the automatic/manual installation method condition
  sleep 2

  # Get the IP address and implement it to the settings file
  echo "${MESSAGES[$MESSAGE_COUNTER]}"
  ((MESSAGE_COUNTER++))
  IP_ADDRESS="$(hostname -I | cut -d' ' -f1)"
  sed -i "s,LOCAL_IP,\"$IP_ADDRESS\",g" service/settings.py
  sleep 2

  echo "${MESSAGES[$MESSAGE_COUNTER]}"
  ((MESSAGE_COUNTER++))
  sleep 2

) | zenity --progress --pulsate --title "$TITLE" --auto-close --width=400 --height=200 2>/dev/null

if [ $? -eq 1 ] ; then
  # Display a cancellation message, delete the project folder and exit the program
  zenity --error --title "$TITLE" --text="$CANCEL_MESSAGE" --width=400 --height=200 2>/dev/null
  if  [ -d "$PROJECT_NAME" ]
  then rm -r "$PROJECT_NAME"
  fi
  exit 0
else
  # Install the docker architecture
  zenity --info --width 300 --title "$TITLE" --text "$DOCKER_BUILD_MESSAGE" 2>/dev/null
  cd "$PROJECT_NAME"
  docker-compose -f prod.yml build
  docker-compose -f prod.yml up -d

  # Display a success info message and exit the program
  zenity --info --width 300 --title "$TITLE" --text "$SUCCESS_MESSAGE" 2>/dev/null
  exit 0
fi
